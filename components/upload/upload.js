import { list } from 'postcss';
import { useEffect, useRef } from 'react'

var $ = window.jQuery = require('jquery');
var _filedrop = require('./filedrop.js');
var p, drop_area;
export default function Upload(props) {
  p = props;
  let dropElement
  useEffect(() => {
    if (drop_area) {
      drop_area.updateData({
        isdir: 0,
        folder: p.path.replace(/^\/disk$/, '/').replace(/^\/disk/, '')
      });
    }
  },[props.path]);
  useEffect(() => {
    if (typeof document == 'undefined')return false;
    window.jQuery(() => {
      var dropbox = $(dropElement)
      var message = $('.message', dropbox)
      var dir = '/';
      drop_area = dropbox.filedrop({
        // The name of the $_FILES entry:
        paramname: 'file',
        data: { isdir: 0, folder: p.path },
        maxfiles: 5,
        maxfilesize: 10,
        url: '/api/upload',
        uploadFinished: function (i, file, response) {
          console.log(response)
          $.data(file).append('<div class="upres-tip">' + (response.msg || '上传失败') + '</div>')
          // response is the JSON object that post_file.php returns
        },

        error: function (err, file) {
          switch (err) {
            case 'BrowserNotSupported':
              showMessage('Your browser does not support HTML5 file uploads!')
              break
            case 'TooManyFiles':
              alert('Too many files! Please select 5 at most! (configurable)')
              break
            case 'FileTooLarge':
              alert(file.name + ' is too large! Please upload files up to 10mb (configurable).')
              break
            default:
              break
          }
        },

        // Called before each upload is started
        beforeEach: function (file) {
          if (!file.type.match(/^image\//)) {
            alert('Only images are allowed!')

            // Returning false will cause the
            // file to be rejected
            return true || false
          }
        },

        uploadStarted: function (i, file, len) {
          createImage(file)
        },

        progressUpdated: function (i, file, progress) {
          $.data(file)
            .find('.progress')
            .width(progress + '%')
        }
      })

      var template =
        '<div class="preview">' +
        '<span class="imageHolder">' +
        '<img />' +
        '<span class="uploaded"></span>' +
        '</span>' +
        '<div class="progressHolder">' +
        '<div class="progress"></div>' +
        '</div>' +
        '</div>'

      function createImage(file) {
        var preview = $(template),
          image = $('img', preview)

        var reader = new FileReader()

        image.width = 40
        image.height = 40

        reader.onload = function (e) {
          // e.target.result holds the DataURL which
          // can be used as a source of the image:

          image.attr('src', e.target.result)
        }

        // Reading the file as a DataURL. When finished,
        // this will trigger the onload function above:
        reader.readAsDataURL(file)

        message.hide()
        preview.appendTo(dropbox)

        // Associating a preview container
        // with the file, using jQuery's $.data():

        $.data(file, preview)
      }

      function showMessage(msg) {
        message.html(msg)
      }
    })
  }, [])

  return (
    <div id='dropbox' ref={(el) => (dropElement = el)}>
      <span className='message'>点击此处或把图片拖拽到此处完成上传</span>
    </div>
  )
}
