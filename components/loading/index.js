import css from './loading.module.css'

export default function loadConfig(props) {
  return (
    <div className={css.loading}>
      <div className='w-full h-full flex flex-col items-center justify-center'>
        <p className={css.h1} data-content='ZARD'>
          ZARD
        </p>
        <div className='mt-1'>正在加载中请稍后...</div>
      </div>
    </div>
  )
}
