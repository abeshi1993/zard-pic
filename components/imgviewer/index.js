import css from './imgviewer.module.css'
import { useState, useEffect } from 'react'

export default function ImgViewer({ lists, current, show = 0, onClose = () => {} }) {
  const [scale, setScale] = useState(1)
  const [rotate, setRotate] = useState(0) // 0 90deg 180deg 270deg
  const rotateStep = 90
  const scaleStep = 0.2

  const [picIndex, setPicIndex] = useState(-1)
  const [picCurrent, setPicCurrent] = useState('')
  const prev = lists[picIndex - 1]
  const next = lists[picIndex + 1]

  const handleZoomin = (e) => {
    if (scale >= 2) return
    setScale(scale + scaleStep)
  }
  const handleZoomout = (e) => {
    if (scale - 0.2 < 1e-3) return // 存在浮点数误差
    setScale(scale - scaleStep)
  }
  const handleRotateLeft = (e) => {
    setRotate((rotate - rotateStep) % 360)
  }
  const handleRotateRight = (e) => {
    setRotate((rotate + rotateStep) % 360)
  }
  const handleRestore = (e) => {
    setScale(1)
    setRotate(0)
  }
  const handleClose = (e) => {
    handleRestore()
    onClose()
    setPicIndex(-1)
  }
  const handlePrev = (e) => {
    if (picIndex < 1) return
    setPicIndex(picIndex - 1)
  }
  const handleNext = (e) => {
    if (picIndex >= lists.length - 1) return

    setPicIndex(picIndex + 1)
  }
  useEffect(() => {
    setPicIndex(lists.indexOf(current))
  }, [lists, current])
  useEffect(() => {
    setPicCurrent(lists[picIndex])
  }, [picIndex])
  return (
    <div tabIndex='-1' className={css['el-image-viewer__wrapper']} style={{ zIndex: 100, display: show ? '' : 'none' }}>
      <div className={css['el-image-viewer__mask']}></div>
      <span className={`${css['el-image-viewer__btn']} ${css['el-image-viewer__close']}`}>
        <i className={css['el-icon']} onClick={handleClose}>
          <svg className={css.icon} width='200' height='200' viewBox='0 0 1024 1024' xmlns='http://www.w3.org/2000/svg'>
            {/**全屏 */}
            <path
              fill='currentColor'
              d='M764.288 214.592L512 466.88 259.712 214.592a31.936 31.936 0 00-45.12 45.12L466.752 512 214.528 764.224a31.936 31.936 0 1045.12 45.184L512 557.184l252.288 252.288a31.936 31.936 0 0045.12-45.12L557.12 512.064l252.288-252.352a31.936 31.936 0 10-45.12-45.184z'
            ></path>
            {/**恢复一比一大小 */}
          </svg>
        </i>
      </span>
      <span className={`${css['el-image-viewer__btn']} ${css['el-image-viewer__prev']}`}>
        <i className={css['el-icon']} onClick={handlePrev}>
          <svg className={css.icon} width='200' height='200' viewBox='0 0 1024 1024' xmlns='http://www.w3.org/2000/svg'>
            <path
              fill='currentColor'
              d='M609.408 149.376L277.76 489.6a32 32 0 000 44.672l331.648 340.352a29.12 29.12 0 0041.728 0 30.592 30.592 0 000-42.752L339.264 511.936l311.872-319.872a30.592 30.592 0 000-42.688 29.12 29.12 0 00-41.728 0z'
            ></path>
          </svg>
        </i>
      </span>
      <span className={`${css['el-image-viewer__btn']} ${css['el-image-viewer__next']}`}>
        <i className={css['el-icon']} onClick={handleNext}>
          <svg
            className={css['icon']}
            width='200'
            height='200'
            viewBox='0 0 1024 1024'
            xmlns='http://www.w3.org/2000/svg'
          >
            <path
              fill='currentColor'
              d='M340.864 149.312a30.592 30.592 0 000 42.752L652.736 512 340.864 831.872a30.592 30.592 0 000 42.752 29.12 29.12 0 0041.728 0L714.24 534.336a32 32 0 000-44.672L382.592 149.376a29.12 29.12 0 00-41.728 0z'
            ></path>
          </svg>
        </i>
      </span>
      <div className={`${css['el-image-viewer__btn']} ${css['el-image-viewer__actions']}`}>
        <div className={css['el-image-viewer__actions__inner']}>
          <i className={css['el-icon']} onClick={handleZoomout}>
            <svg
              className={css.icon}
              width='200'
              height='200'
              viewBox='0 0 1024 1024'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                fill='currentColor'
                d='M795.904 750.72l124.992 124.928a32 32 0 01-45.248 45.248L750.656 795.904a416 416 0 1145.248-45.248zM480 832a352 352 0 100-704 352 352 0 000 704zM352 448h256a32 32 0 010 64H352a32 32 0 010-64z'
              ></path>
            </svg>
          </i>
          <i className={css['el-icon']} onClick={handleZoomin}>
            <svg
              className={css['icon']}
              width='200'
              height='200'
              viewBox='0 0 1024 1024'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                fill='currentColor'
                d='M795.904 750.72l124.992 124.928a32 32 0 01-45.248 45.248L750.656 795.904a416 416 0 1145.248-45.248zM480 832a352 352 0 100-704 352 352 0 000 704zm-32-384v-96a32 32 0 0164 0v96h96a32 32 0 010 64h-96v96a32 32 0 01-64 0v-96h-96a32 32 0 010-64h96z'
              ></path>
            </svg>
          </i>
          <i className={css['el-image-viewer__actions__divider']}></i>
          <i className={css['el-icon']} onClick={handleRestore}>
            <svg
              className={css['icon']}
              width='200'
              height='200'
              viewBox='0 0 1024 1024'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                fill='currentColor'
                d='M813.176 180.706a60.235 60.235 0 0160.236 60.235v481.883a60.235 60.235 0 01-60.236 60.235H210.824a60.235 60.235 0 01-60.236-60.235V240.94a60.235 60.235 0 0160.236-60.235h602.352zm0-60.235H210.824A120.47 120.47 0 0090.353 240.94v481.883a120.47 120.47 0 00120.47 120.47h602.353a120.47 120.47 0 00120.471-120.47V240.94a120.47 120.47 0 00-120.47-120.47zm-120.47 180.705a30.118 30.118 0 00-30.118 30.118v301.177a30.118 30.118 0 0060.236 0V331.294a30.118 30.118 0 00-30.118-30.118zm-361.412 0a30.118 30.118 0 00-30.118 30.118v301.177a30.118 30.118 0 1060.236 0V331.294a30.118 30.118 0 00-30.118-30.118zM512 361.412a30.118 30.118 0 00-30.118 30.117v30.118a30.118 30.118 0 0060.236 0V391.53A30.118 30.118 0 00512 361.412zM512 512a30.118 30.118 0 00-30.118 30.118v30.117a30.118 30.118 0 0060.236 0v-30.117A30.118 30.118 0 00512 512z'
              ></path>
            </svg>
          </i>
          <i className={css['el-image-viewer__actions__divider']}></i>
          <i className={css['el-icon']} onClick={handleRotateLeft}>
            <svg
              className={css['icon']}
              width='200'
              height='200'
              viewBox='0 0 1024 1024'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                fill='currentColor'
                d='M289.088 296.704h92.992a32 32 0 010 64H232.96a32 32 0 01-32-32V179.712a32 32 0 0164 0v50.56a384 384 0 01643.84 282.88 384 384 0 01-383.936 384 384 384 0 01-384-384h64a320 320 0 10640 0 320 320 0 00-555.712-216.448z'
              ></path>
            </svg>
          </i>
          <i className={css['el-icon']} onClick={handleRotateRight}>
            <svg
              className={css.icon}
              width='200'
              height='200'
              viewBox='0 0 1024 1024'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                fill='currentColor'
                d='M784.512 230.272v-50.56a32 32 0 1164 0v149.056a32 32 0 01-32 32H667.52a32 32 0 110-64h92.992A320 320 0 10524.8 833.152a320 320 0 00320-320h64a384 384 0 01-384 384 384 384 0 01-384-384 384 384 0 01643.712-282.88z'
              ></path>
            </svg>
          </i>
        </div>
      </div>
      <div className={css['el-image-viewer__canvas']}>
        {prev && (
          <img
            src={prev}
            className={css['el-image-viewer__img']}
            style={{
              transform: `scale(${scale}) rotate(${rotate}deg)`,
              marginLeft: '0px',
              marginTop: '0px',
              maxHeight: '100%',
              maxWidth: '100%',
              display: 'none'
            }}
          />
        )}
        <img
          src={picCurrent}
          className={css['el-image-viewer__img']}
          style={{
            transform: `scale(${scale}) rotate(${rotate}deg)`,
            marginLeft: '0px',
            marginTop: '0px',
            maxHeight: '100%',
            maxWidth: '100%',
            transition: 'transform 0.3s ease 0s'
          }}
        />
        {next && (
          <img
            src={next}
            className={css['el-image-viewer__img']}
            style={{
              transform: 'scale(1) rotate(0deg)',
              marginLeft: '0px',
              marginTop: '0px',
              maxHeight: '100%',
              maxWidth: '100%',
              display: 'none'
            }}
          />
        )}
      </div>
    </div>
  )
}
