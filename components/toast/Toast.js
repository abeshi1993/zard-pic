import { useEffect, useState, useImperativeHandle, forwardRef } from 'react'

const getPostionCls = (postion = 'center') => {
  let cls = 0
  switch (postion) {
    case 'top':
      cls = 0
      break
    case 'center':
      cls = '50%'
      break
    default:
      cls = '50%'
  }
  return cls
}
const getTypeIcon = (type = 'info') => {
  if (type == 'error') {
    return <i className='icon-times-circle-o text-large text-red'></i>
  }
  if (type == 'warn') {
    ;<i className='icon-warning text-large text-yellow'></i>
  }
  return <i className='icon-info-circle text-large text-blue'></i>
}
const Toast = (props, ref) => {
  //{ text = '提示信息', size = '1rem', color = '#fff', position = 'center', show = true, time }) {
  const [notices, setNotices] = useState([])
  const show = !notices?.length
  useImperativeHandle(ref, () => ({
    setNotices
  }))

  return (
    <div
      className='fixed w-full h-full top-0 flex justify-center mask'
      style={{ transition: 'all 2s .ease 1s', visibility: show ? 'hidden' : '', zIndex: 999 }}
    >
      {notices.map(({ type = 'info', color = '#fff', size = '1rem', text = '提示', position }, id) => {
        return (
          <div
            key={id}
            className='absolute px-3 py-2 size-2 rounded-sm justify-center'
            style={{ background: 'rgba(0,0,0,.8)', color, top: getPostionCls(position) }}
          >
            {getTypeIcon(type)}
            {text}
          </div>
        )
      })}
    </div>
  )
}

export default Toast
