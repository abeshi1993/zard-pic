√-2022/1/18完成Toast组件，可以直接静态方法调用
Toast.show({text: '', html: '', showMask: true, color: '#f00', position: [top|center]})
Toast.clearAll()

-2022/1/19完成对文件的删除操作，数据库和百度网盘操作(可以异步)
-增加一个登录操作，登录后可以放开删除操作，权限管理(允许上传的目录，以及需要密码才能访问的目录等)
√-以及图片放大功能(img:hover{cursor: zoom-in})，包括旋转，下载操作
-改造上传组件，允许多文件分步上传，(后期看能不能改造大文件分片上传)

// 继续完成对图片的删除功能,直接把权限存在本地localStorage里加密方便操作

测试
# Vercel + Planetscale
A [next.js](https://nextjs.org/) app deployed to [vercel](https://vercel.com) with a [planetscale](https://planetscale.com) integration. This example app uses the [planetscale-node](https://github.com/planetscale/planetscale-node) client to query the database

## Setup database
- Install [Planetscale CLI](https://planetscale.com/cli)
- Authenticate the CLI
```sh
pscale auth login
```
- Create a new database (_skip this step if you already have a database_)
```sh
pscale database create <database>
```
- Create a `development` branch
```sh
pscale branch create <database> <branch>
```
- Connect to your branch
```sh
pscale shell <database> <branch>
```
- Insert example tables
```sql
CREATE TABLE users (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  email varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  name varchar(255)
);
```
- Create a **deploy request** 
```bash
pscale deploy-request create <database> <branch>
```
- _Deploy_ the **deploy request** 
```bash
pscale deploy-request deploy <database> <deploy-request-number>
```
- To find your `<deploy-request-number>`, simply run:
```bash
pscale deploy-request list <database>
```
- Merge your `development` branch into `main`
```bash
pscale deploy-request deploy <database> <deploy-request-number>
```

## Clone & Deploy to vercel
<a href="https://vercel.com/new/git/external?repository-url=https%3A%2F%2Fgithub.com%2Fplanetscale%2Fvercel-integration-example&project-name=vercel-integration-example&repository-name=vercel-integration-example&integration-ids=oac_ni8CGiTU3oM25q1k2L6unVMp">
  <img src="https://vercel.com/button" alt="Deploy with Vercel"/>
</a>


- The integration will automatically add the following environment variables to your Vercel project(s)
  - `PLANETSCALE_DB`
  - `PLANETSCALE_ORG`
  - `PLANETSCALE_TOKEN`
  - `PLANETSCALE_TOKEN_NAME`

_These environment variables are used by [planetscale-node](https://github.com/planetscale/planetscale-node) client to connect to your database_
- Re-deploy your application after the installation is complete and you will have a working app.
