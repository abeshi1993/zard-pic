/* eslint-disable prettier/prettier */
import css from './header.module.css'

export default function Header(props) {
    let path = '/pic/zard';
    let paths = path.split('/');
  return (
    <div>
      {/**<!---顶部logo-->**/}
      <div className={css['nav-top']}>
        <div className={css['logo-img']}>
          <img src='https://avatars.githubusercontent.com/u/3286367?s=96&v=4' width={60} height={60} />
        </div>
        <div className={css['logo-title']}>ZARD</div>
        {props.children}
      </div>
    </div>
  )
}
