import '../styles/globals.css'
import '../styles/pintuer.css'
import '../styles/filedrop.css'



if (typeof window == 'undefined') {
  global.window = {};
}
if (typeof global.navigator === 'undefined') global.navigator = {};

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
