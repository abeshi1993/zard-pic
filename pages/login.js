import { useState, useEffect } from 'react'

// 内存泄漏测试
var m
export default function Login() {
  const [value, setValue] = useState(0)
  var replaceThing = function () {
    var originalThing = m
    var unused = function () {
      if (originalThing) {
        console.log('hi')
      }
    }
    m = { longStr: new Array(100000).join('***'), a() {} }
  }
  return (
    <div>
      Counter: {value}
      <button onClick={(e) => setValue(value + 1)}>加1</button>
    </div>
  )
}
