import multiparty from 'multiparty'
import conn from './utils/conn'
import Pan from './utils/pan'


export default async function Upload(req, res) {
  const form = new multiparty.Form()
  form.parse(req, async (err, fields, files) => {
    if (err) {
      return res.status(500).end('500 请求错误！')
    }
    const [folder] = fields.folder
    const { path, originalFilename } = files.file[0]
    //const file_stream = fs.readFileSync(path);

    const file_name = originalFilename
    const pan = new Pan(process.env.BAIDU_ACCESS_TOKEN)
    let fs_id, file_size
    try {
      const [res2, err2] = await pan.uploadFile(file_name, path, folder)
      if (err2) {
        res.status(500).json(err2)
      } else {
        fs_id = res2.fs_id
        file_size = res2.size
      }
    } catch (err) {
      res.end(JSON.stringify(err))
    }

    // 保存到数据库中
    let sql
    try {
      const time = new Date(new Date(new Date().toGMTString()).getTime() + 8 * 3600 * 1000)
        .toJSON()
        .replace('T', ' ')
        .replace(/\.\d+Z/, '')
      const dir = folder.replace(/^\/disk$/, '/').replace(/^\/disk/, '')
      sql = `insert into file (id, fs_id, file_name, is_dir, folder, size, time) values
    (null, '${fs_id}', '${file_name}', 0, '${dir}', ${file_size}, '${time}')`
      const [rows, fields] = await conn.query(sql)
      res.statusCode = 201
      res.json({ code: 1, msg: '上传成功', fs_id, file_size })
    } catch (err) {
      console.log(err);
      res.statusCode = 500
      res.json({ code: 0, msg: '上传失败', err })
    }
  })
}

// 关闭内置的bodyParser让multiparty解析
export const config = {
  api: {
    bodyParser: false
  }
}
