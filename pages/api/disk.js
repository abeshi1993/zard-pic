// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import conn from './utils/conn'
import Pan from './utils/pan'

import { list_data } from './mockdata'

const pan = new Pan(process.env.BAIDU_ACCESS_TOKEN)

export default async (req, res) => {
  const {
    query: { type },
    method
  } = req
  switch (method) {
    case 'GET':
      if (type == 'list') {
        let { folder = '/disk', page = 0, size = 15 } = req.query
        if (process.env.NODE_ENV == 'development2') {
          return res.json(
            await new Promise((rs, rj) => {
              let data = []
              if (page < 3) {
                data = list_data(15)
              }

              setTimeout(() => {
                rs(data)
              }, 3000)
            })
          )
        }
        try {
          const cursor = (page - 1) * size
          folder = encodeURI(folder)
          const [getRows, _] = await conn.query(
            `select * from file where folder='${folder}' order by time desc limit ${cursor},${size}`
          )
          const fs_ids = getRows.filter((row) => !row.is_dir).map((row) => row.fs_id)
          const [list, error] = await pan.getMultimedias(fs_ids)
          const result = getRows.map((row) => {
            let matchedRow = list.filter((l) => l.fs_id == row.fs_id)[0]
            if (matchedRow) {
              return Object.assign(row, matchedRow)
            }
            return row
          })
          res.status = 200
          res.json(result)
        } catch (e) {
          // 出错后需要更新下access_token
          console.log(e)
          let error = new Error('An error occurred while connecting to the database')
          error.status = 500
          error.info = { message: 'An error occurred while connecting to the database' }
          throw error
        }
      } else if (type == 'create') {
        // 上传目录
        const { isdir, dir, folder } = req.query
        if (!dir || dir.length == 0) {
          return res.json({ code: 0, msg: '不允许的目录名，创建目录失败！' })
        }
        const time = new Date(new Date(new Date().toGMTString()).getTime() + 8 * 3600 * 1000)
          .toJSON()
          .replace('T', ' ')
          .replace(/\.\d+Z/, '')
        let sql = `insert into file(file_name, is_dir, folder, time) values('${dir}', '${isdir}', '${folder}', '${time}')`
        try {
          const [rows, fields] = await conn.query(sql)
          return res.json(rows)
        } catch (err) {
          throw new Error(err)
        }
      } else {
        res.status(403).end('未知参数，403！')
      }
      break
    case 'POST':
      // 仅仅对删除操作进行网盘操作，移动和重命名仅仅对数据库进行操作，复制暂时不需要意义不大
      if (!type || ['del', 'copy', 'rename', 'move'].indexOf(type) < 0) {
        return res.status(403).end('不支持的操作请求!')
      }
      // 先针对删除操作, 需要权限
      let { fs_ids } = req.body
      console.log(typeof fs_ids);
      return res.json({
        status: 1,
        msg: '删除失败:没有权限！'
      })
      try {
        const sql = `delete from file where fs_id in (${fs_ids})`
        //console.log(sql)
        const [rs, error] = await conn.query(sql)
        //console.log(rs)
        if (error) {
          console.log(error)
          return res.json({
            status: 0,
            msg: '删除失败！'
          })
        }
        return res.json({
          status: 1,
          msg: '删除成功！'
        })
      } catch (err) {
        throw new Error(err)
      }
    //if(type == 'del') await pan.manage({ type, filelist })
    default:
      res.setHeader('Allow', ['GET', 'PUT'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }
}
