import fs from 'fs'
import _path from 'path'



const _ROOT = 'E:\\Kugou'
const listDirectory = (dir) => {
  const list = fs.readdirSync(dir)
  const filelist = list
    .map((f) => {
      let filepath = _path.resolve(dir, f)
      return `<li><a href="/api/test?path=${filepath}">${f}</a></li>`
    })
    .join('')
  return `
    <h3>${dir}目录列表:</h3>
    ${filelist}
    `
}
export default async (req, res) => {
  const {
    query: { path }
  } = req
  let list
  if (!path) {
    list = fs.readdirSync(_ROOT)
    res.setHeader('Content-Type', 'text/html; charset=utf8')
    return res.end(listDirectory(_ROOT))
  }
  const filePath = _path.resolve(_ROOT, path)
  try {
    if (fs.statSync(_path.resolve(_ROOT, path)).isDirectory()) {
      // 处理目录
      res.setHeader('Content-Type', 'text/html; charset=utf8')
      return res.end(listDirectory(filePath))
    } else {
      // 处理文件
      fs.createReadStream(filePath).pipe(res)
    }
  } catch(err) {
    res.status(404).end('404 Not Found')
  }
}
